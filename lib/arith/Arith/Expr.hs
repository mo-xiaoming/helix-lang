module Arith.Expr where

data Prim where
  Int :: Int -> Prim
  Float :: Float -> Prim
  Double :: Double -> Prim
  deriving (Show)

data Expr where
  Prim :: Prim -> Expr
  Add :: Expr -> Expr -> Expr
  Neg :: Expr -> Expr
  Mul :: Expr -> Expr -> Expr
  Inv :: Expr -> Expr
  deriving (Show)
