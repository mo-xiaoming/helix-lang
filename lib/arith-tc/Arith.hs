-- |
module Arith where

import Arith.Expr (Expr)
import Arith.Parse.Monad (parse)
import Arith.Parser (arith)
import Arith.Tc.Infer (infer, run)
import GHC.Exts (IsString (..))
import Optics

instance IsString Expr where
  fromString = either (error . show) id . parse arith

-- $> "+ [1, 2, 3]" :: Expr

-- $> "+ [1]" :: Expr

-- $> "- [1, 2]" :: Expr

-- $> "- [true, 2]" :: Expr

-- $> "- [true, 2]" :: Expr

-- $> ("1" :: Expr) & infer & view run

-- $> ("+ [1, 2]" :: Expr) & infer & view run

-- $> ("- [1, 2]" :: Expr) & infer & view run

-- $> ("- [1.0, 2]" :: Expr) & infer & view run

-- $> ("- [t]" :: Expr) & infer & view run

-- $> ("if true then 1 else 2" :: Expr) & infer & view run
