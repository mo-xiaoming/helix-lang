{-# LANGUAGE StandaloneDeriving #-}

module Arith.Tc.Expr where

import Arith.Op
import Arith.Tc.Type (Ty)
import Data.Sequence (Seq)

data P a where
  Bool :: Bool -> P Bool
  Int :: Int -> P Int
  Float :: Float -> P Float
  Double :: Double -> P Double

deriving instance Show (P a)

data E a where
  Prim :: P a -> E a
  Bin :: Bin -> Seq (E a) -> E a
  Una :: Una -> E a -> E a
  If :: E Bool -> E a -> E a -> E a

deriving instance Show (E a)

data Expr where
  Expr :: Ty a -> E a -> Expr

deriving instance Show Expr
