module Arith.Op.Una where

data Una where
  (:-:) :: Una
  (:/:) :: Una
  (:!:) :: Una
  deriving (Enum, Eq, Bounded, Show)
