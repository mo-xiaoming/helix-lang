module Arith.Op.Bin where

data Bin where
  (:+:) :: Bin
  (:*:) :: Bin
  (:=:) :: Bin
  (:<:) :: Bin
  (:<=:) :: Bin
  (:>:) :: Bin
  (:>=:) :: Bin
  deriving (Enum, Eq, Bounded, Show)
