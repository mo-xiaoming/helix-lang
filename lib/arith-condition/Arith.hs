-- |
module Arith where

import Arith.Expr (Expr)
import Arith.Parse.Monad (parse)
import Arith.Parser (arith)
import GHC.Exts (IsString (..))

instance IsString Expr where
  fromString = either (error . show) id . parse arith

-- $> "+ [1, 2, 3]" :: Expr

-- $> "+ [1]" :: Expr

-- $> "- [1, 2]" :: Expr

-- $> "- [true, 2]" :: Expr
