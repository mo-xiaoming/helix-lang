module Arith.Expr where

import Data.Sequence (Seq)

data Prim where
  Bool :: Bool -> Prim
  Int :: Int -> Prim
  Float :: Float -> Prim
  Double :: Double -> Prim
  deriving (Show)

data Bin where
  (:+:) :: Bin
  (:*:) :: Bin
  (:=:) :: Bin
  (:<:) :: Bin
  (:<=:) :: Bin
  (:>:) :: Bin
  (:>=:) :: Bin
  deriving (Enum, Bounded, Show)

data Una where
  (:-:) :: Una
  (:/:) :: Una
  (:!:) :: Una
  deriving (Enum, Bounded, Show)

data Expr where
  Prim :: Prim -> Expr
  Bin :: Bin -> Seq Expr -> Expr
  Una :: Una -> Expr -> Expr
  If :: Expr -> Expr -> Expr -> Expr
  deriving (Show)
