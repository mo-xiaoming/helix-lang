{
  description = "HeLiX programming language with Happy parsing examples";
  inputs = {
    np.url = "github:nixos/nixpkgs?branch=haskell-upgrade";
    fu = {
      url = "github:numtide/flake-utils?branch=master";
      inputs.nipkgs.follows = "np";
    };
    hls = {
      url = "github:haskell/haskell-language-server?branch=1.4.0-hackage";
      inputs.nipkgs.follows = "np";
    };
  };

  outputs = { self, np, fu, hls }:
    with fu.lib;
    with np.lib;
    eachSystem [ "x86_64-linux" ] (system:
      let
        version =
          "${substring 0 8 self.lastModifiedDate}-${self.shortRev or "dirty"}";
        config = { };
        overlay = final: _:
          with final;
          with haskell.lib;
          with haskellPackages.extend (final: _:
            with final; rec {
              optics-core = callHackage "optics-core" "0.4" { };
              optics-extra =
                callHackage "optics-extra" "0.4" { inherit optics-core; };
              optics-vl =
                callHackage "optics-vl" "0.2.1" { inherit optics-core; };
              optics-th =
                callHackage "optics-th" "0.4" { inherit optics-core; };
              optics = callHackage "optics" "0.4" {
                inherit optics-core optics-th optics-extra;
              };
            }); {
              helix-lang = (callCabal2nix "helix-lang" ./. { }).overrideAttrs
                (prev: { version = "${prev.version}-${version}"; });
            };
        overlays = [ overlay hls.overlay ];
      in with (import np { inherit system config overlays; }); rec {
        inherit overlays;
        devShell = with haskellPackages;
          shellFor {
            packages = _: [ ];
            buildInputs =
              [ cabal-install ghc haskell-language-server happy alex ];
          };
        packages = flattenTree (recurseIntoAttrs { inherit helix-lang; });
        defaultPackage = packages.helix-lang;
      });
}
